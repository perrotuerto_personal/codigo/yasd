# YASD, Yet Another Schema Definition

YASD is a YAML format for human writable XSDs (XML Schema Definition), humans
declare what is indispensable, leaving the machines to do the rest of the
unreadable `<syntaxis who_can_read_this="?" />`.

## CLI Workflow

The following chart is the YASD CLI workflow:

``` mermaid
flowchart LR
    direction LR
    yasd --- convert
    yasd --- check
    yasd --- sample
    yasd --- document
    yasd --- man
    convert --- |from| YASD1([YASD])
    YASD1 --> |to| XSD([XSD])
    check --- |validates| YASD2([YASD])
    sample --- |from| YASD3([YASD])
    YASD3 --> |to| XML([XML])
    document --- |from| YASD4([YASD])
    YASD4 --> |to| RST([reStructuredText])
    man --- |prints| README
```

## Example

To better understad what YASD does, see the inputs and outputs:

-   Input: human workable schema in [YASD].
-   Output: computer processable schema in [XSD].
-   Output: human readable documentation in [reStructuredText].

## Table of Contents

\[TOC\]

## Structure

### General Structure

    schema:
      SCHM 
    elements:
      - ELMT
      ...
    attributeElements:
      - ATTR
      ...
    groups:
      - GRPS

### Schema (SCHM) Structure

    elementFormDefault: qualified|unqualified
    targetNamespace: http://a.link
    xmlns: http://a.link
    schemaLocation: http://a-link-to.xsd
    version: 0.1

### Element (ELMT) Structure

    name: element_name
    description: Element description
    type: simple|empty|no_text|no_elements|mixed 
    datatype: string|integer|decimal|date|time|language|duration|token|boolean|byte|int|double|float|long|short|normalizedString|dateTime|gDay|gMonth|gMonthDay|gYear|gYearMonth|negativeInteger|nonNegativeInteger|nonPositiveInteger|positiveInteger|unsignedLong|unsignedInt|unsignedShort|unsignedByte|anyURI|base64Binary|hexBinary|Name|QName|NCName|ID|IDREF|IDREFS|ENTITY|ENTITIES|NMTOKEN|NMTOKENS|NOTATION
    attributes:
      - ref: attribute_name
        use: optional|required|prohibited
      - group: group_name
      ...
    children_order: all|choice|sequence
    children:
      - ref: element_name
        maxOccurs: INTEGER|unbounded
        minOccurs: INTEGER
      - group: group_name
        maxOccurs: INTEGER|unbounded
        minOccurs: INTEGER
        remove:
          - element_or_attribute_name
      ...

### Attribute (ATTR) Structure

    name: attribute_name
    description: Attribute description
    datatype: string|integer|decimal|date|time|language|duration|token|boolean|byte|int|double|float|long|short|normalizedString|dateTime|gDay|gMonth|gMonthDay|gYear|gYearMonth|negativeInteger|nonNegativeInteger|nonPositiveInteger|positiveInteger|unsignedLong|unsignedInt|unsignedShort|unsignedByte|anyURI|base64Binary|hexBinary|Name|QName|NCName|ID|IDREF|IDREFS|ENTITY|ENTITIES|NMTOKEN|NMTOKENS|NOTATION
    default: a_value
    fixed: a_value
    restriction:
      - CONSTRAIN
      ...

### Group (GRPS) Structure

    name: group_name
    attribute_group: true|false
    children_order: all|choice|sequence 
    children:
      - ref: element_or_attribute_name
        maxOccurs: INTEGER|unbounded
        minOccurs: INTEGER

## Key Reference

### `attribute_group`

Indicates if group is an attribute group.

Optional; if not present by default is `false`.

Allowed values:

-   `true`.
-   `false`.

### `attributeElements`

Indicates attributes elements for schema.

Optional.

### `attributes`

Indicates a list of attributes for an element.

### `children_order`

Indicates order indicators for children elements.

Mandatory in `no_text` or `mixed` elements, and group.

Allowed values:

-   `all`. Children elements can occur in any order.
-   `choice`. Only one children element can accur.
-   `sequence`. Children elements must occur in specified order.

### `children`

Indicates a list of children elements.

Mandatory in `no_text` or `mixed` elements, and group.

### `datatype`

Indicates element or attribute data types.

Only mandatory for 'simple' and 'no_elements' elements, and attributes.

Allowed String Data Types:

-   `ENTITIES`.  
-   `ENTITY`.  
-   `ID`. A string that represents the ID attribute in XML (only used with
    schema attributes).
-   `IDREF`. A string that represents the IDREF attribute in XML (only used
    with schema attributes).
-   `IDREFS`.
-   `language`. A string that contains a valid language id.
-   `Name`. A string that contains a valid XML name.
-   `NCName`.
-   `NMTOKEN`. A string that represents the NMTOKEN attribute in XML (only used
    with schema attributes).
-   `NMTOKENS`.
-   `normalizedString`. A string that does not contain line feeds, carriage
    returns, or tabs.
-   `QName`.
-   `string`. A string.
-   `token`. A string that does not contain line feeds, carriage returns, tabs,
    leading or trailing spaces, or multiple spaces.

Allowed Date and Time Data Types:

-   `date`. Defines a date value.
-   `dateTime`. Defines a date and time value.
-   `duration`. Defines a time interval.
-   `gDay`. Defines a part of a date - the day (DD).
-   `gMonth`. Defines a part of a date - the month (MM).
-   `gMonthDay`. Defines a part of a date - the month and day (MM-DD).
-   `gYear`. Defines a part of a date - the year (YYYY).
-   `gYearMonth`. Defines a part of a date - the year and month (YYYY-MM).
-   `time`. Defines a time value.

Allowed Numeric Data Types:

-   `byte`. A signed 8-bit integer.
-   `decimal`. A decimal value.
-   `int`. A signed 32-bit integer.
-   `integer`. An integer value.
-   `long`. A signed 64-bit integer.
-   `negativeInteger`. An integer containing only negative values (..,-2,-1).
-   `nonNegativeInteger`. An integer containing only non-negative values
    (0,1,2,..).
-   `nonPositiveInteger`. An integer containing only non-positive values
    (..,-2,-1,0).
-   `positiveInteger`. An integer containing only positive values (1,2,..).
-   `short`. A signed 16-bit integer.
-   `unsignedLong`. An unsigned 64-bit integer.
-   `unsignedInt`. An unsigned 32-bit integer.
-   `unsignedShort`. An unsigned 16-bit integer.
-   `unsignedByte`. An unsigned 8-bit integer.

Allowed Miscellaneous Data Types:

-   `anyURI`.
-   `base64Binary`.
-   `boolean`.
-   `double`.
-   `float`.
-   `hexBinary`.
-   `NOTATION`.
-   `QName`.

### `default`

Indicates default value when element or attribute is empty.

Optional.

Only allowed for simple elements or attributes.

### `description`

Indicates element or attribute description in human readable form.

Optional.

### `elementFormDefault`

Indicates that any elements used by the XML instance document which were
declared in this schema must be namespace qualified.

Optional; if not present by default is `unqualified`.

### `elements`

Indicates elements for schema.

Mandatory.

### `fixed`

Indicates fixed value to element or attribute.

Optional; ignored if 'default' is present.

Only allowed for simple elements or attributes.

### `group`

References group name.

Optional.

### `groups`

Indicates element or attribute groups for schema.

Optional.

### `maxOccurs`

Indicates max number of times a children element can accur.

Optional; if not present by default is `1`.

Valid values are non negative integer or `unbounded` for unlimited number of
times.

### `minOccurs`

Indicates min number of times a children element can accur.

Optional; if not present by default is `1`.

Valid value is non negative integer.

### `name`

Indicates element, attribute or group name.

Mandatory.

For elements, its name is commonly known as tag name.

Naming rules:

-   Element names are case-sensitive
-   Element names must start with a letter or underscore
-   Element names cannot start with the letters xml (or XML, or Xml, etc)
-   Element names can contain letters, digits and underscores
-   Element names cannot contain spaces or hyphens

### `ref`

References element or attribute by name.

Mandatory.

### `remove`

Removes element or attribute from group by name.

Optional.

### `restriction`

Indicates accepted constrained values for attribute.

Optional; if present, must contain at least one constrain and attribute
`datatype` is ignored.

Allowed constrains:

-   `enumeration`. Specifies a list of acceptable values.
-   `fractionDigits`. Specifies the maximum number of decimal places allowed;
    must be equal to or greater than zero.
-   `length`. Specifies the exact number of characters or list items allowed;
    must be equal to or greater than zero.
-   `maxExclusive`. Specifies the upper bounds for numeric values (the value
    must be less than this value).
-   `maxInclusive`. Specifies the upper bounds for numeric values (the value
    must be less than or equal to this value).
-   `maxLength`. Specifies the maximum number of characters or list items
    allowed; must be equal to or greater than zero.
-   `minExclusive`. Specifies the lower bounds for numeric values (the value
    must be greater than this value).
-   `minInclusive`. Specifies the lower bounds for numeric values (the value
    must be greater than or equal to this value).
-   `minLength`. Specifies the minimum number of characters or list items
    allowed; must be equal to or greater than zero.
-   `pattern`. Defines the exact sequence of characters that are acceptable.
-   `totalDigits`. Specifies the exact number of digits allowed; must be
    greater than zero.
-   `whiteSpace`. Specifies how white space (line feeds, tabs, spaces, and
    carriage returns) is handled; accepted values are
    `preserve|replace|collapse`.

### `schema`

Indicates schema general information.

Mandatory.

### `schemaLocation`

Indicates the location of the XML schema to use for that namespace.

Optional.

### `taget_namespace`

Indicates that the elements defined by this schema come from the specified URL
namespace.

Optional.

### `type`

Indicates element type.

Mandatory.

Allowed types:

-   `simple`. Only text node allowed.
-   `empty`. Only attributes allowed.
-   `no_text`. No children text nodes allowed.
-   `no_elements`. No children elements allowed.
-   `mixed`. Children elements, text node and attributes allowed.

Chart:

    | type        | elements | text | attributes |
    |-------------|:--------:|:----:|:----------:|
    | simple      | ✗        | ✓    | ✗          |
    | empty       | ✗        | ✗    | ✓          |
    | no_text     | ✓        | ✗    | ✓          |
    | no_elements | ✗        | ✓    | ✓          |
    | mixed       | ✓        | ✓    | ✓          |

> **Note 1**: read "elements" and "text" with "direct children..." as prefix.

> **Note 2**: attributes are never mandatory; they could be zero or more.

### `use`

Indicates that the attribute is required.

Optional; if not present by default is `optional`.

Allowed uses:

-   `optional`.
-   `required`.
-   `prohibited`.

### `version`

Indicates XML schema version.

Mandatory.

### `xmlns`

Indicates that the default namespace is the specified URL.

Mandatory.

## References

-   "XML Schema Reference", [W3ref]
-   "XML Schema Tutorial", [W3Schools].
-   "XSD Tutorial", [Tutorials Point].

## License

YASD is under GPLv3, check the license terms [here].

## Acknowledgments

YASD is founded by [Mexican Academy of Language].

## License

YASD is under GPLv3, check the license terms \[here\]

  [YASD]: https://gitlab.com/amlengua/apal/esquema/-/blob/main/apal.yaml
  [XSD]: https://gitlab.com/amlengua/apal/esquema/-/blob/main/apal.xsd
  [reStructuredText]: https://gitlab.com/amlengua/apal/esquema/-/blob/main/apal.rst
  [W3ref]: https://www.w3schools.com/xml/schema_elements_ref.asp
  [W3Schools]: https://www.w3schools.com/xml/schema_intro.asp
  [Tutorials Point]: https://www.tutorialspoint.com/xsd/
  [here]: https://git.cuates.net/perro/yasd/src/branch/no-masters/LICENSE.md
  [Mexican Academy of Language]: https://academia.org.mx
